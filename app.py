from flask import Flask
import redis

app = Flask('app')
storage = redis.StrictRedis('redis')

@app.route('/')
def get():
    return '🎊'

@app.route('/<x>', methods=['PUT'])
def put(x):
    storage.lpush('list', x)
    return '🏀'

@app.route('/', methods=['TRACE'])
def trace():
    list = [x.decode('utf-8') for x in storage.lrange('list', 0, -1)]
    return ', '.join(list[:-1]) + f" and {list[-1]}"

@app.route('/', methods=['PATCH'])
def patch():
    1 / 0
    return '🐜'
